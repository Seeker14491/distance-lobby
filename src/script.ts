interface ServerData { readonly servers: Server[] }

interface Server {
    readonly serverName: string;
    readonly mode: string;
    readonly connectedPlayers: number;
    readonly playerLimit: number;
    readonly passwordProtected: boolean;
    readonly build: number;
}

interface Settings {
    hidePrivate: boolean;
    buildFilter: string;
}

const tableBody = document.getElementById('tableBody')!;
const hidePrivateCheckbox = document.getElementById('hidePrivate')! as HTMLInputElement;
const buildFilterBox = document.getElementById('buildFilter')! as HTMLInputElement;
const statusIndicator = document.getElementById('statusIndicator')! as HTMLDivElement;

let serverData: ServerData = { servers: [] };

function getFormattedText(s: string): string {
    return s.replace(/\[([\da-f]{6})\](.*)/gi, function(match, p1, p2) {
        return `<span style="color: #${p1}">${getFormattedText(p2)}</span>`;
    });
}

function onSettingsChanged(): void {
    saveSettings();
    updateTableBody();
}

function updateTableBody(): void {
    let out = '';
    let serversDisplayed = 0;
    for (const server of serverData.servers) {
        if ((hidePrivateCheckbox.checked && server.passwordProtected) ||
            (buildFilterBox.value !== '' && server.build !== +buildFilterBox.value)) {
            continue;
        }

        const status = server.passwordProtected ? 'Private' : 'Public';
        const fullColor = (server.connectedPlayers < server.playerLimit) ? 'lime' : 'red';

        out +=
            `<tr>
            <td>${getFormattedText(server.serverName)}</td>
            <td>${server.mode}</td>
            <td><font color="${fullColor}">${server.connectedPlayers}/${server.playerLimit}</font></td>
            <td>${status}</td>
            <td>${server.build}</td>
            </tr>`

        ++serversDisplayed;
    }

    tableBody.innerHTML = out;
    
    const totalServers = serverData.servers.length;
    const serversNotDisplayed = totalServers - serversDisplayed;
    if (totalServers === 0) {
        statusIndicator.innerHTML = 'No Servers';
    } else if (serversDisplayed === totalServers) {
        statusIndicator.innerHTML = '';
    } else if (serversDisplayed === 0) {
        statusIndicator.innerHTML = `No servers (${serversNotDisplayed} server${serversNotDisplayed > 1 ? 's' : ''} hidden by filters)`;
    } else {
        statusIndicator.innerHTML = `+${serversNotDisplayed} server${serversNotDisplayed > 1 ? 's' : ''} hidden by filters`;
    }
}

function update(): void {
    fetch('http://35.185.40.23/')
    .then((response) => response.json())
    .then((data) => {
        serverData = data;
        updateTableBody();
    })
    .catch(() => statusIndicator.innerHTML = 'Error fetching servers');
}

function loop(): void {
    update();
    setTimeout(loop, 10000);
}

function loadSettings(): void {
    const item = window.localStorage.getItem('settings');
    const settings: Settings = (item === null) ? getDefaultSettings() : JSON.parse(item);
    hidePrivateCheckbox.checked = settings.hidePrivate;
    buildFilterBox.value = settings.buildFilter;
}

function getDefaultSettings(): Settings {
    return {
        hidePrivate: false,
        buildFilter: "",
    };
}

function saveSettings(): void {
    const settings = {
        hidePrivate: hidePrivateCheckbox.checked,
        buildFilter: buildFilterBox.value,
    };
    window.localStorage.setItem('settings', JSON.stringify(settings));
}

loadSettings();
hidePrivateCheckbox.addEventListener('change', onSettingsChanged);
buildFilterBox.addEventListener('input', onSettingsChanged);
loop();
