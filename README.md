This is a web app that displays the currently hosted [Distance]() game servers. This is a frontend to [this server](https://github.com/Seeker14491/distance-lobby-list-server).

### [open](http://distance.rip/) (hosted by [triazo](https://github.com/triazo))



### Building

##### Setting the IP

Before building, you might want to change the IP of the [backend server](https://github.com/Seeker14491/distance-lobby-list-server). It's currently hardcoded in `src/script.ts`, so edit the line

```
fetch('http://35.185.40.23/')
```

to the desired IP.



To build the web app you need have npm installed, and then run these commands in this directory:

```
npm install
npm run build
```

The built web app will then be inside the `build` directory.